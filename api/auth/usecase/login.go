package authUsecase

import (
	"context"
	"errors"
	authDomainEntity "test-case-majoo/api/auth/domain/entity"
	authDomainInterface "test-case-majoo/api/auth/domain/interface"
	authRepository "test-case-majoo/api/auth/repository"
	conversionHelper "test-case-majoo/helper/conversion"
	jwtAuth "test-case-majoo/package/auth/jwt"
	"test-case-majoo/package/config"
	"test-case-majoo/package/manager"

	"github.com/golang-jwt/jwt"
	"github.com/rs/zerolog/log"
)

type Login struct {
	repo authDomainInterface.LoginRepository
	jwt  jwtAuth.Jwt
	cfg  *config.Config
}

func NewLoginUsecase(mgr manager.Manager) authDomainInterface.LoginUsecase {
	usecase := new(Login)
	usecase.repo = authRepository.NewLoginRepository(mgr.GetGorm())
	usecase.jwt = mgr.GetJwt()
	usecase.cfg = mgr.GetConfig()

	return usecase
}

func (uc *Login) Login(ctx context.Context, req *authDomainEntity.User) (*authDomainEntity.ResultUser, error) {
	dataUser, err := uc.repo.Login(ctx, req)
	if err != nil {
		code := "[Usecase] Login-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	hash, _ := conversionHelper.HashPassword(req.Password)
	match := conversionHelper.CheckPasswordHash(dataUser.Password, hash)
	if !match {
		code := "[Usecase] Login-2"
		log.Error().Err(err).Msg(code)
		err = errors.New("2002")
		return nil, err
	}

	return dataUser, nil
}

func (uc *Login) GenerateToken(ctx context.Context, userID int) (*authDomainEntity.Token, error) {
	data, err := uc.repo.GetUserByUserID(ctx, userID)
	if err != nil {
		code := "[Usecase] GenerateToken-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	// generate jwt with code id
	jwtData := &jwtAuth.JwtData{
		UserID:          data.UserID,
		UserName:        data.Name,
		RepresentedID:   data.RepresentedID,
		RepresentedName: data.RepresentedName,
		StandardClaims: jwt.StandardClaims{
			Id:        string(userID),
			NotBefore: jwt.TimeFunc().Local().Unix(),
		},
	}
	accessToken, refreshToken, err := uc.jwt.GenerateToken(jwtData)
	if err != nil {
		code := "[Usecase] GenerateToken-2"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	token := &authDomainEntity.Token{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
	return token, nil
}

func (uc *Login) VerifyRefreshToken(ctx context.Context, req *authDomainEntity.Token) (string, error) {
	jwtID, err := uc.jwt.VerifyRefreshToken(req.RefreshToken)
	if err != nil {
		code := "[Usecase] VerifyAccessToken-1"
		log.Error().Err(err).Msg(code)
		return "", err
	}

	return jwtID, nil
}
