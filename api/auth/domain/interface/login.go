package authDomainInterface

import (
	"context"
	"net/http"

	authDomainEntity "test-case-majoo/api/auth/domain/entity"
)

type LoginHandler interface {
	Login() http.Handler
	RefreshToken() http.Handler
}

type LoginUsecase interface {
	Login(ctx context.Context, req *authDomainEntity.User) (*authDomainEntity.ResultUser, error)
	GenerateToken(ctx context.Context, userID int) (*authDomainEntity.Token, error)
	VerifyRefreshToken(ctx context.Context, req *authDomainEntity.Token) (string, error)
}

type LoginRepository interface {
	Login(ctx context.Context, req *authDomainEntity.User) (*authDomainEntity.ResultUser, error)
	GetUserByUserID(ctx context.Context, userID int) (*authDomainEntity.DataInToken, error)
}
