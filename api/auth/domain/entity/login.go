package authDomainEntity

import errorHelper "test-case-majoo/helper/error"

type Token struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type DataInToken struct {
	UserID          string `json:"uid" gorm:"column:uid"`
	Name            string `json:"uname" gorm:"column:uname"`
	RepresentedID   string `json:"rid" gorm:"column:rid"`
	RepresentedName string `json:"rname" gorm:"column:rname"`
}

type User struct {
	UserName string `json:"user_name" gorm:"user_name"`
	Password string `json:"password" gorm:"password"`
}

type ResultUser struct {
	Username     string `json:"user_name"`
	Name         string `json:"name"`
	Password     string `json:"password"`
	UserID       int    `json:"user_id"`
	MerchantID   int    `json:"merchant_id"`
	MerchantName string `json:"merchant_name"`
}

func (o *Token) ValidateToken() error {
	if o.RefreshToken == "" {
		return errorHelper.ErrorDataCannotEmpty("refresh_token")
	}

	return nil
}

func (o *User) ValidateUser() error {
	if o.UserName == "" {
		return errorHelper.ErrorDataCannotEmpty("user_name")
	}

	if o.Password == "" {
		return errorHelper.ErrorDataCannotEmpty("password")
	}

	return nil
}
