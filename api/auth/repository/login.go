package authRepository

import (
	"context"
	"fmt"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"

	authDomainEntity "test-case-majoo/api/auth/domain/entity"
	authDomainInterface "test-case-majoo/api/auth/domain/interface"
	errorHelper "test-case-majoo/helper/error"
)

type login struct {
	DB *gorm.DB
}

func NewLoginRepository(database *gorm.DB) authDomainInterface.LoginRepository {
	repo := new(login)
	repo.DB = database

	return repo
}

func (repo *login) Login(ctx context.Context, req *authDomainEntity.User) (*authDomainEntity.ResultUser, error) {
	result := &authDomainEntity.ResultUser{}
	err := repo.DB.Raw(`
		SELECT a.user_name as username, a.name as name, a.password as password, a.id as user_id, b.id as merchant_id, b.merchant_name as merchant_name
		FROM Users as a
		LEFT JOIN Merchants as b ON b.user_id = a.id
		WHERE a.user_name = ?
	`, req.UserName).Find(&result).Error

	if err != nil {
		code := "[Repository] Login-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	if result == nil {
		code := "[Repository] Login-2"
		err = errorHelper.ErrorDataNotExist("User", fmt.Sprintf("username = %s", req.UserName))
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return result, nil
}

func (repo *login) GetUserByUserID(ctx context.Context, userID int) (*authDomainEntity.DataInToken, error) {
	var count int64
	var data *authDomainEntity.DataInToken
	err := repo.DB.Table("Users").Raw(`
		SELECT 
			a.id as uid,
			a.name as uname,
		 	b.id as rid,
			b.merchant_name as rname
		FROM Users as a
		LEFT JOIN Merchants as b ON b.user_id = a.id
		WHERE a.id = ?
	`, userID).Find(&data).Count(&count).Error
	if err != nil {
		code := "[Repository] GetUserByUserID-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	if count < 1 {
		code := "[Repository] GetUserByUserID-2"
		err = errorHelper.ErrorDataNotExist("User", fmt.Sprintf("id = %d", userID))
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return data, nil
}
