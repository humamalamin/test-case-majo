package authRoute

import (
	authHandler "test-case-majoo/api/auth/delivery/handler"
	"test-case-majoo/package/manager"

	"github.com/gorilla/mux"
)

func NewLoginRoute(mgr manager.Manager, route *mux.Router) {
	LoginHandler := authHandler.NewLoginHandler(mgr)

	route.Handle("/login", LoginHandler.Login()).Methods("POST")
	route.Handle("/access-token/refresh", LoginHandler.RefreshToken()).Methods("POST")
}
