package authRoutes

import (
	authRoute "test-case-majoo/api/auth/delivery/route"
	"test-case-majoo/package/manager"

	"github.com/gorilla/mux"
)

func NewRoutes(r *mux.Router, mgr manager.Manager) {
	apiV1Auth := r.PathPrefix("/v1/auth").Subrouter()

	authRoute.NewLoginRoute(mgr, apiV1Auth)
}
