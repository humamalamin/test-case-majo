package authHandler

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	authDomainEntity "test-case-majoo/api/auth/domain/entity"
	authDomainInterface "test-case-majoo/api/auth/domain/interface"
	authUsecase "test-case-majoo/api/auth/usecase"
	jsonHelper "test-case-majoo/helper/json"
	"test-case-majoo/package/manager"

	"github.com/rs/zerolog/log"
)

type Login struct {
	Usecase authDomainInterface.LoginUsecase
}

func NewLoginHandler(mgr manager.Manager) authDomainInterface.LoginHandler {
	handler := new(Login)
	handler.Usecase = authUsecase.NewLoginUsecase(mgr)

	return handler
}

func (h *Login) Login() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req *authDomainEntity.User
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			code := "[Handler] Login-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		err := req.ValidateUser()
		if err != nil {
			code := "[Handler] Login-2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		dataUser, err := h.Usecase.Login(r.Context(), req)
		if err != nil {
			if err.Error() == "2002" {
				code := "[Handler] Login-3"
				log.Error().Err(err).Msg(code)
				jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "Password not match", nil)
				return
			}

			code := "[Handler] Login-3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		newReq, err := h.Usecase.GenerateToken(r.Context(), dataUser.UserID)
		if err != nil {
			code := "[Handler] Login-3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		jsonHelper.SuccessResponse(w, r, true, http.StatusOK, "1000", newReq, "Login success", nil)
	})
}

func (h *Login) RefreshToken() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req *authDomainEntity.Token
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			code := "[Handler] RefreshToken-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		err := req.ValidateToken()
		if err != nil {
			code := "[Handler] RefreshToken-2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		jwtID, err := h.Usecase.VerifyRefreshToken(r.Context(), req)
		if err != nil {
			code := "[Handler] RefreshToken-3"
			if err.Error() == "Token is expired" {
				err = errors.New("token is expired")
				log.Error().Err(err).Msg(code)
				jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, err.Error(), nil)
				return
			}

			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		jwtIDInt, _ := strconv.Atoi(jwtID)

		newReq, err := h.Usecase.GenerateToken(r.Context(), jwtIDInt)
		if err != nil {
			code := "[Handler] RefreshToken-4"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		jsonHelper.SuccessResponse(w, r, true, http.StatusOK, "1000", newReq, "Success refresh token", nil)
	})
}
