package reportRepository

import (
	"context"
	reportDomainEntity "test-case-majoo/api/report/domain/entity"
	reportDomainInterface "test-case-majoo/api/report/domain/interface"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

type report struct {
	DB *gorm.DB
}

func NewReportRepository(database *gorm.DB) reportDomainInterface.ReportRepository {
	repo := new(report)
	repo.DB = database

	return repo
}

func (repo *report) DayOfMonthReport(ctx context.Context, param reportDomainEntity.ParamRequest) ([]*reportDomainEntity.ReportMerchant, error) {
	results := []*reportDomainEntity.ReportMerchant{}
	err := repo.DB.Raw(`
		SELECT b.merchant_name as merchant_name, IFNULL(SUM(a.bill_total), 0) as omzet,
			DATE_FORMAT(a.created_at, "%Y-%m-%d") as date_of_month
		FROM Transactions AS a
		LEFT JOIN Merchants AS b ON a.merchant_id = b.id
		LEFT JOIN Users AS c ON b.user_id = c.id
		WHERE a.merchant_id = ? AND MONTH(a.created_at) = 11
		GROUP BY date_of_month, merchant_name
		ORDER BY date_of_month ASC;
	`, param.MerchantID).Scan(&results).Error

	if err != nil {
		code := "[Repository] DayOfMonthReport-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return results, nil
}

func (repo *report) DayOfMontReportOulet(ctx context.Context, param reportDomainEntity.ParamRequest) ([]*reportDomainEntity.ReportOutlet, error) {
	results := []*reportDomainEntity.ReportOutlet{}
	err := repo.DB.Raw(`
		SELECT b.merchant_name, d.outlet_name,
			IFNULL(SUM(a.bill_total), 0) AS omzet, DATE_FORMAT(a.created_at, "%Y-%m-%d")as date_of_month
		FROM Transactions AS a
		LEFT JOIN Merchants AS b ON a.merchant_id = b.id
		LEFT JOIN Users AS c ON b.user_id = c.id
		LEFT JOIN Outlets AS d ON d.merchant_id = b.id AND a.outlet_id = d.id
		WHERE c.id = ?
		GROUP BY date_of_month, outlet_name, merchant_name
		ORDER BY date_of_month ASC;
	`, param.UserID).Scan(&results).Error

	if err != nil {
		code := "[Repository] DayOfMontReportOulet-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return results, nil
}

func (repo *report) GetMerchantByID(ctx context.Context, merchantID int64) (reportDomainEntity.Merchant, error) {
	result := reportDomainEntity.Merchant{}
	err := repo.DB.Table("Merchants").Where("id = ?", merchantID).Take(&result).Error
	if err != nil {
		code := "[Repository] GetMerchantByID-1"
		log.Error().Err(err).Msg(code)
		return result, err
	}

	return result, nil
}
