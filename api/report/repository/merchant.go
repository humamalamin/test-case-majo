package reportRepository

import (
	"context"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"

	merchantDomainEntity "test-case-majoo/api/report/domain/entity"
	merchantDomainInterface "test-case-majoo/api/report/domain/interface"
)

type merchant struct {
	DB *gorm.DB
}

func NewMerchantRepository(database *gorm.DB) merchantDomainInterface.MerchantRepository {
	repo := new(merchant)
	repo.DB = database

	return repo
}

func (repo *merchant) Get(ctx context.Context) ([]*merchantDomainEntity.Merchant, error) {
	results := []*merchantDomainEntity.Merchant{}

	err := repo.DB.Table("Merchants").Find(results).Error
	if err != nil {
		code := "[Repository] GetMerchant-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return results, nil
}

func (repo *merchant) GetByID(ctx context.Context, merchantID int64) (merchantDomainEntity.Merchant, error) {
	result := merchantDomainEntity.Merchant{}
	err := repo.DB.Table("Merchants").Where("id = ?", merchantID).Take(&result).Error
	if err != nil {
		code := "[Repository] GetMerchantByID-1"
		log.Error().Err(err).Msg(code)
		return result, err
	}

	return result, nil
}
