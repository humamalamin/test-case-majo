package reportDomainInterface

import (
	"context"

	merchantDomainEntity "test-case-majoo/api/report/domain/entity"
)

type MerchantUsecase interface {
	Get(ctx context.Context) ([]*merchantDomainEntity.Merchant, error)
	GetByID(ctx context.Context, merchantID int64) (merchantDomainEntity.Merchant, error)
}

type MerchantRepository interface {
	Get(ctx context.Context) ([]*merchantDomainEntity.Merchant, error)
	GetByID(ctx context.Context, merchantID int64) (merchantDomainEntity.Merchant, error)
}
