package reportDomainInterface

import (
	"context"
	"net/http"

	reportDomainEntity "test-case-majoo/api/report/domain/entity"
)

type ReportHandler interface {
	DayOfMonthReport() http.Handler
	DayOfMontReportOulet() http.Handler
}

type ReportUsecase interface {
	DayOfMonthReport(ctx context.Context, param reportDomainEntity.ParamRequest) ([]*reportDomainEntity.ReportMerchant, error)
	DayOfMontReportOulet(ctx context.Context, param reportDomainEntity.ParamRequest) ([]*reportDomainEntity.ReportOutlet, error)
}

type ReportRepository interface {
	DayOfMonthReport(ctx context.Context, param reportDomainEntity.ParamRequest) ([]*reportDomainEntity.ReportMerchant, error)
	DayOfMontReportOulet(ctx context.Context, param reportDomainEntity.ParamRequest) ([]*reportDomainEntity.ReportOutlet, error)
	GetMerchantByID(ctx context.Context, merchantID int64) (reportDomainEntity.Merchant, error)
}
