package reportDomainEntity

import "time"

type Merchant struct {
	ID           int64     `json:"id"`
	UserID       int64     `json:"user_id"`
	MerchantName string    `json:"merchant_name"`
	CreatedAt    time.Time `json:"created_at"`
	CreatedBy    int64     `json:"created_by"`
	UpdatedAt    time.Time `json:"updated_at"`
	UpdatedBy    int64     `json:"updated_by"`
}

type Outlet struct {
	ID         int64  `json:"id"`
	MerchantID int64  `json:"merchant_id"`
	OutletName string `json:"outlet_name"`
}
