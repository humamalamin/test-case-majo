package reportDomainEntity

type ParamRequest struct {
	MerchantID int64  `json:"merchant_id"`
	UserID     int64  `json:"user_id"`
	Page       string `json:"page"`
	PerPage    string `json:"per_page"`
}

type ReportMerchant struct {
	MerchantName string `json:"merchant_name"`
	Omzet        int64  `json:"omzet"`
	DateOfMonth  string `json:"date_of_month"`
}

type ResultReportMerchant struct {
	MerchantName string `json:"merchant_name"`
	Omzet        string `json:"omzet"`
	DateOfMonth  string `json:"date_of_month"`
}

type ReportOutlet struct {
	MerchantName string `json:"merchant_name"`
	OutletName   string `json:"outlet_name"`
	Omzet        int64  `json:"omzet"`
	DateOfMonth  string `json:"date_of_month"`
}

type ResultReportOutlet struct {
	MerchantName string `json:"merchant_name"`
	OutletName   string `json:"outlet_name"`
	Omzet        string `json:"omzet"`
	DateOfMonth  string `json:"date_of_month"`
}
