package reportDeliveryRoute

import (
	reportHandler "test-case-majoo/api/report/delivery/handler"
	"test-case-majoo/package/manager"

	"github.com/gorilla/mux"
)

func NewReportRoute(mgr manager.Manager, route *mux.Router) {
	reportHandler := reportHandler.NewReportHandler(mgr)
	route.Use(mgr.GetMiddleware().CheckToken)

	route.Handle("/merchants", reportHandler.DayOfMonthReport()).Methods("GET")
	route.Handle("/outlets", reportHandler.DayOfMontReportOulet()).Methods("GET")
}
