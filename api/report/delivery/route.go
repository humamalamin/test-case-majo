package reportRoutes

import (
	reportRoute "test-case-majoo/api/report/delivery/route"
	"test-case-majoo/package/manager"

	"github.com/gorilla/mux"
)

func NewRoutes(r *mux.Router, mgr manager.Manager) {
	apiV1Auth := r.PathPrefix("/v1/reports").Subrouter()

	reportRoute.NewReportRoute(mgr, apiV1Auth)
}
