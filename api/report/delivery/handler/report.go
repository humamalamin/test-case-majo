package reportHandler

import (
	"net/http"
	reportDomainEntity "test-case-majoo/api/report/domain/entity"
	reportDomainInterface "test-case-majoo/api/report/domain/interface"
	reportUsecase "test-case-majoo/api/report/usecase"
	conversionHelper "test-case-majoo/helper/conversion"
	jsonHelper "test-case-majoo/helper/json"
	paginationHelper "test-case-majoo/helper/pagination"
	middlewareAuth "test-case-majoo/package/auth/middleware"
	"test-case-majoo/package/manager"
	"time"

	"github.com/rs/zerolog/log"
)

type Report struct {
	Usecase    reportDomainInterface.ReportUsecase
	Middleware middlewareAuth.Middleware
	Pagination paginationHelper.Pagination
}

func NewReportHandler(mgr manager.Manager) reportDomainInterface.ReportHandler {
	handler := new(Report)
	handler.Usecase = reportUsecase.NewReportUsecase(mgr)
	handler.Middleware = middlewareAuth.NewMiddleware(mgr.GetConfig())
	handler.Pagination = mgr.GetPagination()

	return handler
}

func (h *Report) DayOfMonthReport() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		userInfo, err := h.Middleware.GetUserInfoFromContext(ctx)
		if err != nil {
			code := "[Handler] DayOfMonthReport-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, err.Error(), nil)
			return
		}

		page := r.FormValue("page")
		if page == "" {
			page = "1"
		}

		perPage := r.FormValue("per_page")
		if perPage == "" {
			perPage = "10"
		}

		param := reportDomainEntity.ParamRequest{
			MerchantID: userInfo.RepresentedID,
			UserID:     userInfo.UserID,
			Page:       page,
			PerPage:    perPage,
		}

		results, err := h.Usecase.DayOfMonthReport(ctx, param)
		if err != nil {
			code := "[Handler] DayOfMonthReport-2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		pagination, err := h.Pagination.AddPagination(len(results), page, perPage)
		if err != nil {
			code := "[Handler] DayOfMonthReport-3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		finalResults := []*reportDomainEntity.ResultReportMerchant{}

		for _, res := range results {
			omzet := conversionHelper.ConvertToRpCurrency(res.Omzet)
			date, _ := time.Parse("2006-01-02", res.DateOfMonth)
			index := reportDomainEntity.ResultReportMerchant{
				MerchantName: res.MerchantName,
				Omzet:        omzet,
				DateOfMonth:  date.Format("02 January 2006"),
			}

			finalResults = append(finalResults, &index)
		}

		finalResults = finalResults[pagination.First:pagination.Last]

		jsonHelper.SuccessResponse(w, r, true, http.StatusOK, "1000", finalResults, "success", pagination)
	})
}

func (h *Report) DayOfMontReportOulet() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		userInfo, err := h.Middleware.GetUserInfoFromContext(ctx)
		if err != nil {
			code := "[Handler] DayOfMontReportOulet-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, err.Error(), nil)
			return
		}

		page := r.FormValue("page")
		if page == "" {
			page = "1"
		}

		perPage := r.FormValue("per_page")
		if perPage == "" {
			perPage = "10"
		}

		param := reportDomainEntity.ParamRequest{
			MerchantID: userInfo.RepresentedID,
			UserID:     userInfo.UserID,
			Page:       page,
			PerPage:    perPage,
		}

		results, err := h.Usecase.DayOfMontReportOulet(ctx, param)
		if err != nil {
			code := "[Handler] DayOfMontReportOulet-2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		pagination, err := h.Pagination.AddPagination(len(results), page, perPage)
		if err != nil {
			code := "[Handler] DayOfMontReportOulet-3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, err.Error(), nil)
			return
		}

		finalResults := []*reportDomainEntity.ResultReportOutlet{}

		for _, res := range results {
			omzet := conversionHelper.ConvertToRpCurrency(res.Omzet)
			date, _ := time.Parse("2006-01-02", res.DateOfMonth)
			index := reportDomainEntity.ResultReportOutlet{
				OutletName:   res.OutletName,
				MerchantName: res.MerchantName,
				Omzet:        omzet,
				DateOfMonth:  date.Format("02 January 2006"),
			}

			finalResults = append(finalResults, &index)
		}

		finalResults = finalResults[pagination.First:pagination.Last]

		jsonHelper.SuccessResponse(w, r, true, http.StatusOK, "1000", finalResults, "success", pagination)
	})
}
