package reportUsecase

import (
	"context"
	merchantDomainEntity "test-case-majoo/api/report/domain/entity"
	merchantDomainInterface "test-case-majoo/api/report/domain/interface"
	merchantRepository "test-case-majoo/api/report/repository"
	"test-case-majoo/package/config"
	"test-case-majoo/package/manager"

	"github.com/rs/zerolog/log"
)

type Merchant struct {
	repo merchantDomainInterface.MerchantRepository
	cfg  *config.Config
}

func NewMerchantUsecase(mgr manager.Manager) merchantDomainInterface.MerchantUsecase {
	usecase := new(Merchant)
	usecase.repo = merchantRepository.NewMerchantRepository(mgr.GetGorm())
	usecase.cfg = mgr.GetConfig()

	return usecase
}

func (uc *Merchant) Get(ctx context.Context) ([]*merchantDomainEntity.Merchant, error) {
	results, err := uc.repo.Get(ctx)
	if err != nil {
		code := "[Usecase] GetMerchant-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return results, nil
}

func (uc *Merchant) GetByID(ctx context.Context, merchantID int64) (merchantDomainEntity.Merchant, error) {
	result, err := uc.repo.GetByID(ctx, merchantID)
	if err != nil {
		code := "[Usecase] GetMerchantByID-1"
		log.Error().Err(err).Msg(code)
		return result, err
	}

	return result, nil
}
