package reportUsecase

import (
	"context"
	reportDomainEntity "test-case-majoo/api/report/domain/entity"
	reportDomainInterface "test-case-majoo/api/report/domain/interface"
	reportRepository "test-case-majoo/api/report/repository"
	"test-case-majoo/package/config"
	"test-case-majoo/package/manager"
	"time"

	"github.com/rs/zerolog/log"
)

type Report struct {
	repo reportDomainInterface.ReportRepository
	cfg  *config.Config
}

func NewReportUsecase(mgr manager.Manager) reportDomainInterface.ReportUsecase {
	usecase := new(Report)
	usecase.repo = reportRepository.NewReportRepository(mgr.GetGorm())
	usecase.cfg = mgr.GetConfig()

	return usecase
}

func (uc *Report) DayOfMonthReport(ctx context.Context, param reportDomainEntity.ParamRequest) ([]*reportDomainEntity.ReportMerchant, error) {
	results, err := uc.repo.DayOfMonthReport(ctx, param)
	if err != nil {
		code := "[Usecase] DayOfMonthReport-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	dataMerchant, err := uc.repo.GetMerchantByID(ctx, param.MerchantID)
	if err != nil {
		code := "[Usecase] DayOfMonthReport-2"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	date := "2021-11-01"
	newDate, _ := time.Parse("2006-01-02", date)
	currentYear, currentMonth, _ := newDate.Date()
	currentLocation := newDate.Location()
	firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	lastOfMonth := firstOfMonth.AddDate(0, 1, -1)
	countDate := lastOfMonth.Day()
	listResults := []*reportDomainEntity.ReportMerchant{}

	listResults2 := make(map[string]*reportDomainEntity.ReportMerchant, len(results))
	for _, rs := range results {
		listResults2[rs.DateOfMonth] = rs
	}

	for i := 0; i < countDate; i++ {
		items := reportDomainEntity.ReportMerchant{}
		dateNow := newDate.AddDate(0, 0, i)
		newFormat := dateNow.Format("2006-01-02")
		if listResults2[newFormat] == nil {
			items.MerchantName = dataMerchant.MerchantName
			items.Omzet = 0
			items.DateOfMonth = newFormat
		} else {
			items.DateOfMonth = listResults2[newFormat].DateOfMonth
			items.MerchantName = listResults2[newFormat].MerchantName
			items.Omzet = listResults2[newFormat].Omzet
		}

		listResults = append(listResults, &items)
	}

	return listResults, nil
}

func (uc *Report) DayOfMontReportOulet(ctx context.Context, param reportDomainEntity.ParamRequest) ([]*reportDomainEntity.ReportOutlet, error) {
	results, err := uc.repo.DayOfMontReportOulet(ctx, param)
	if err != nil {
		code := "[Usecase] DayOfMontReportOulet-1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	dataMerchant, err := uc.repo.GetMerchantByID(ctx, param.MerchantID)
	if err != nil {
		code := "[Usecase] DayOfMonthReport-2"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	date := "2021-11-01"
	newDate, _ := time.Parse("2006-01-02", date)
	currentYear, currentMonth, _ := newDate.Date()
	currentLocation := newDate.Location()
	firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	lastOfMonth := firstOfMonth.AddDate(0, 1, -1)
	countDate := lastOfMonth.Day()
	listResults := []*reportDomainEntity.ReportOutlet{}

	listResults2 := make(map[string]*reportDomainEntity.ReportOutlet, len(results))
	for _, rs := range results {
		listResults2[rs.DateOfMonth] = rs
	}

	for i := 0; i < countDate; i++ {
		items := reportDomainEntity.ReportOutlet{}
		dateNow := newDate.AddDate(0, 0, i)
		newFormat := dateNow.Format("2006-01-02")
		if listResults2[newFormat] == nil {
			items.MerchantName = dataMerchant.MerchantName
			items.Omzet = 0
			items.DateOfMonth = newFormat
			items.OutletName = "-"
		} else {
			items.DateOfMonth = listResults2[newFormat].DateOfMonth
			items.MerchantName = listResults2[newFormat].MerchantName
			items.Omzet = listResults2[newFormat].Omzet
			items.OutletName = listResults2[newFormat].OutletName
		}

		listResults = append(listResults, &items)
	}

	return listResults, nil
}
