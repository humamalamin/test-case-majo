package errorHelper

type ErrResponse struct {
	Code Message
}

type Message struct {
	MessageBE string
	Code      string
}
