package main

import (
	"fmt"
	"os"
	"test-case-majoo/package/manager"
	"test-case-majoo/package/server"
	"time"

	authRoutes "test-case-majoo/api/auth/delivery"
	reportRoutes "test-case-majoo/api/report/delivery"
)

func run() error {
	mgr, err := manager.NewInit()
	if err != nil {
		return err
	}

	tzLocation, err := time.LoadLocation(mgr.GetConfig().AppTz)
	if err != nil {
		return err
	}
	time.Local = tzLocation

	server := server.NewServer(mgr.GetConfig())
	server.Router.Use(mgr.GetMiddleware().InitLog)

	// routes
	authRoutes.NewRoutes(server.Router, mgr)
	reportRoutes.NewRoutes(server.Router, mgr)

	server.RegisterRouter(server.Router)

	return server.ListenAndServe()
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}

}
