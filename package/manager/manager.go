package manager

import (
	"database/sql"
	"os"
	paginationHelper "test-case-majoo/helper/pagination"
	jwtAuth "test-case-majoo/package/auth/jwt"
	middlewareAuth "test-case-majoo/package/auth/middleware"
	gocacheCache "test-case-majoo/package/cache/go-cache"
	httpClient "test-case-majoo/package/client"
	"test-case-majoo/package/config"
	gormDatabase "test-case-majoo/package/database/gorm"
	mysqlDatabase "test-case-majoo/package/database/mysql"
	"test-case-majoo/package/server"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

type Manager interface {
	GetConfig() *config.Config
	GetServer() *server.Server
	GetMysql() *sql.DB
	GetGorm() *gorm.DB
	GetHttp() httpClient.Http
	GetJwt() jwtAuth.Jwt
	GetMiddleware() middlewareAuth.Middleware
	GetPagination() paginationHelper.Pagination
	GetGoCache() gocacheCache.GoCache
}

type manager struct {
	config         *config.Config
	server         *server.Server
	dbMysql        *sql.DB
	dbGorm         *gorm.DB
	httpClient     httpClient.Http
	jwtAuth        jwtAuth.Jwt
	middlewareAuth middlewareAuth.Middleware
	pagination     paginationHelper.Pagination
	goCacheCache   gocacheCache.GoCache
}

func NewInit() (Manager, error) {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Error().Err(err).Msg("[NewInit-1] Failed to Initialize Configuration")
		return nil, err
	}

	srv := server.NewServer(cfg)

	dbMysql, err := mysqlDatabase.NewMysql(cfg).Connect()
	if err != nil {
		log.Error().Err(err).Msg("[NewInit-2] Failed to Initialize Database Mysql")
		return nil, err
	}

	dbGorm, err := gormDatabase.NewGorm(cfg).Connect()
	if err != nil {
		log.Error().Err(err).Msg("[NewInit-3] Failed to Initialize Database Gorm")
		return nil, err
	}
	clHttp := httpClient.NewHttp(cfg)
	clHttp.Connect()

	jwt := jwtAuth.NewJwt(cfg)
	middleware := middlewareAuth.NewMiddleware(cfg)

	log.Logger = log.With().Caller().Logger()
	if cfg.AppIsDev {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: "2006-01-02 15:04:05"}).With().Caller().Logger()
	}

	paginationHelper := paginationHelper.NewPagination()

	goCache := gocacheCache.NewGoCache()

	return &manager{
		config:         cfg,
		server:         srv,
		dbMysql:        dbMysql,
		dbGorm:         dbGorm,
		httpClient:     clHttp,
		jwtAuth:        jwt,
		middlewareAuth: middleware,
		pagination:     paginationHelper,
		goCacheCache:   goCache,
	}, nil
}

func (sm *manager) GetConfig() *config.Config {
	return sm.config
}

func (sm *manager) GetServer() *server.Server {
	return sm.server
}

func (sm *manager) GetMysql() *sql.DB {
	return sm.dbMysql
}

func (sm *manager) GetGorm() *gorm.DB {
	return sm.dbGorm
}

func (sm *manager) GetHttp() httpClient.Http {
	return sm.httpClient
}

func (sm *manager) GetJwt() jwtAuth.Jwt {
	return sm.jwtAuth
}

func (sm *manager) GetMiddleware() middlewareAuth.Middleware {
	return sm.middlewareAuth
}

func (sm *manager) GetPagination() paginationHelper.Pagination {
	return sm.pagination
}

func (sm *manager) GetGoCache() gocacheCache.GoCache {
	return sm.goCacheCache
}
