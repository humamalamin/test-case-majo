package jwtAuth

import "github.com/golang-jwt/jwt"

type JwtData struct {
	UserID          string `json:"uid"`
	UserName        string `json:"uname"`
	RepresentedID   string `json:"rid"`
	RepresentedName string `json:"rname"`
	jwt.StandardClaims
}
