## About Apps

This apps is API for resolve test case backend from majoo. I make this API use Go Language and has implemented clean architecture.

### Pre-require
- Go ^v1.7
- Mysql/MariaDB
- Vscode

### Install Apps

```bash 
    git@gitlab.com:humamalamin/test-case-majo.git
    rename file local.env.example to local.env
    config value file local.env
    make run
```